<?php 
/* Same as WF College Two except has conditional statement
** which loads blog entries of all categories on front page, blog
** entries with category "Other Student Awards on Student Awards 
** page, blog entries with category "Conferences" on Conferences
** page, blog entries with category Student Publications" on Student
** Publications page, and blog entries with category Alumni on
** Alumni page. */

/**
 * The template used for displaying page content in page.php and page-nosidebar.php
 *
 * @package WF College Two
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'wf-college-two' ),
				'after'  => '</div>',
			) );
		?>
		<!-- display list of sub-pages, if the post content is empty -->
						<?php
						  $children = wp_list_pages( "title_li=&child_of=".$post->ID."&echo=0&depth=1" );
						  if ( empty( $post->post_content ) ) {
						 ?>
						  <p class="menu-placeholder-page">For those on a touchscreen device, here are the child pages of this page: </p>
						  <ul>
						  <?php the_title('<h3>', ' Sub-pages</h3>'); ?> 
						  <?php echo $children; ?>
						  </ul>
						  <?php }
						  else
						?>
<!-- end of show children bit (from codex.wordpress.org) -->

	</div><!-- .entry-content -->

	<footer class="entry-footer">
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
<?php
/* Conditional checks to put correct blog entries at bottom of page, if appropriate. */
if( is_page( array( 'Alumni', 'Student Publications', 'Awards', 'Conference Participation', 'Home' ) ) ) {
	if( is_front_page() ) : $currentcategory = '';
		elseif( is_page( 'Alumni' ) ) : $currentcategory = 'alumni';
		elseif( is_page( 'Student Publications' ) ) : $currentcategory = 'student-publications';
		elseif( is_page( 'Awards' ) ) : $currentcategory = 'other-student-awards';
		elseif( is_page( 'Conference Participation' ) ) : $currentcategory = 'conferences';
		endif; 
	$args = array( 
		'post_type' => 'post',
		'posts_per_page' => 5,
		'category_name' => $currentcategory,			
		);
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post(); 
		get_template_part( 'content' ); //content.php contains basic blog format
	  endwhile;
	  }
