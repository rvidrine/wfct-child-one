<?php
/* 
** Don't load the hide search script, since search is visible all the time in this design 
** Also, enqueue the child style sheet.
** Enqueueing these scripts instead of using an @import rule can save on HTTP
** calls and also be a benefit when using plugins like Minqueue which help combine
** and minify scripts and styles that have been enqueued.
*/
function wfct_child_one_scripts_and_styles() {
	wp_deregister_script( 'wf-college-two-hide-search' );
	wp_enqueue_style( 'wf-college-two-child-style' , get_stylesheet_directory_uri() . '/style.css');
}
add_action( 'wp_enqueue_scripts', 'wfct_child_one_scripts_and_styles', 11 );
