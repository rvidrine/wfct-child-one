# README #
This is a child theme of WF College Two, and is designed specifically for use by academic departments, faculty and staff of Wake Forest University in Winston Salem, North Carolina, USA.
This repository is public so that anyone interested in the theme can view it and learn from it. Like most of the WordPress ecosystem, this theme is licensed under a very lenient license. However, please do not use this theme in its entirety on a non-WFU website. It is made public so that others can learn from this theme and develop their own skills in creating WordPress themes.

This theme is installed the same way as any other theme in WordPress, although if one downloads the .zip of this archive, they will need to rename the download to wfct-child-one.zip in order for it to install correctly. The parent theme of this theme, WF College Two (also available here as a free repository) must also be installed, as it provides the bulk of styling and functionality to this theme.

Any questions or concerns about this theme can be directed to Robert Vidrine at vidrinmr at wfu dot edu.